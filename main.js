import 'dotenv/config'
import {Telegraf, Markup } from 'telegraf';

const TOKEN = process.env.TG_TOKEN;
const WEB_APP = process.env.WEB_APP;

const bot = new Telegraf(TOKEN);

bot.command('start', (ctx) => {
    ctx.reply('Добро пожаловать во Tg Vue Bot! Нажмите кнопку ниже чтобы запустить приложение',
        Markup.keyboard([Markup.button.webApp(
            'Отправить сообщение',
            WEB_APP
        )]))
})

bot.launch();